#Fix 'Computer Appears to Be Infected' Message When You Access Google in OfficeScan [Case Study]#

Trend Micro OfficeScan antivirus program helps to keep your system free from all sort of threats. A number of Trend Micro users have complained about 'Your Computer Appears to be infected' error message that is displayed at the top of the screen when you try to search on Google.

When you get such an error message, it means your computer system may be affected by a DNSChanger malware. You can resolve this issue, by following the given solutions:

##Solution 1: Alter your DNS server address##

If you use a Static IP address setup, this solution is not for you. In case, your computer system utilizes this setup, you require contacting your Internet Service Provider or Network Administrator or you can proceed to the next solution.
To alter your DNS server address, select your system's OS and follow the instructions.

**Windows XP:**

* At first, follow: Start> Control Panel> Network Connections.
* You will see Local Area Connection, right-click on it and select properties.
* Select Internet Protocol (TCP/IP), followed by the Properties.
* Verify the value of the Preferred, as well as, Alternate DNS server field. Select Automatically Obtain DNS server address if your value is 85.255.xxx.xxx.

**Note:** Here 'xxx' represents a number.

* Click on 'OK' option and close the window. Now, check whether the issue persists.

**Windows 7 and Windows Vista:**

* At first, follow: Start> Control Panel> Network and Internet.
* Select the 'Network and Sharing Center' option.
* Choose 'Manage Network Connections' option.
* You will see Local Area Connection, right-click on it and select properties.
* Select Internet Protocol v.4 (TCP/IPv4), followed by the Properties.
* Verify the value of the Preferred, as well as, Alternate DNS server field. Select Automatically Obtain DNS server address if your value is 85.255.xxx.xxx.

**Note:** Here 'xxx' represents a number.    

* Click on 'OK' option and close the window. Now, check whether the issue persists.

##Solution 2: Flush your DNS##

Follow the given instructions to flush the DNS.

(1) At first, you have to open the Command Prompt.

**Windows XP users, follow:**
 
* Click on Start> Run.
* In the given text field, type in 'cmd' and enter.
    
**Windows 7 and Windows Vista users, follow:**

* Click on Start option.
* Select 'Search the Programs and Files' option in the search field.
* In the given text field, type in 'cmd' and enter.

(2) On the Command Prompt, type in "ipconfig /flushdns" and press the Enter key.

(3) When you will close the Command Prompt, you receive a message stating 'DNS has been flushed'.

(4) Now, check whether the issue is eliminated or not.

##Solution 3: Reset your Browser Settings##

Those who use Firefox, follow:
     * To remove the browser history along with temporary Internet files, clear private data.
     * To restore the original settings of the browser, reset the preferences.

**Those who use Internet Explorer browser, follow:**

To reset the original settings of the Internet Explorer, follow:

* At first, open the Internet Explorer.
* elect Tools> Internet Options.
* Click on the 'Advanced' tab.
* Select the 'Restore Advanced Settings' and click 'Reset'.
* At the end, click on 'Apply', followed by the 'OK' option.

##Solution 4: Look for the presence of Spyware or viruses##

To check whether your computer is infected and to remove any malware (such as spyware and viruses), you can run House Call, an online scanner.

**Follow the given instructions to run an online scan utilizing House Call scanner:**

* At first, click on this House Call Free Online Virus Scan to download the House Call and save it to your desktop.
* Click on 'Run' and it will open the House Call window. Wait till the House Call download its essential components to run on your PC.
     * Please note the speed of downloading depends on your Internet connection speed.
* Go through the license agreement and accept the terms by clicking on 'Next' icon.
* Select the Settings> Scan Type.
* In here, you have to pick which type of scan you want to initiate, then click 'OK'.
* There are 3 types of scan to choose from:
     * Quick Scan- This scan quickly checks the areas where malware (malicious software) may be found.
     * Full Computer System Scan- This scan goes through all the files and folders thoroughly on your computer to look for the presence of any spyware and viruses.
     * Note: This may take a while depending on the file size and quantity of the folders present on your computer.
     * Custom Scan- This scan enables you to select the files and folders you wish to scan.
* Click on 'Scan Now'.
* Wait till Trend Micro House Call finishes the scan. When it will display you a summary of threats, take necessary actions against those detected malware.
* At last, click on the 'Close' option.


**Related Topics: [Simple Way to Resolve Trend Micro Causing Computer to Freeze Problem](http://wendymurphy.page.tl/)**

##Solution 5: Reset your Router DNS and Settings##

If the issue persists, there are chances of changes in your DNS router. In such a situation, you have to directly change or reset the DNS settings of your router. Therefore, go through the manual or support site of your router for further instructions.

Even after going through the above-mentioned instructions, you are not able to troubleshoot 'Computer appears to be infected' error message when you try to access Google in OfficeScan, seek help from the  **[Trend Micro Customer Service](https://customercaretoll.com/antivirus-support/trend-micro-support)** tollfree number.